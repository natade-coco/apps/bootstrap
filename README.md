<div align="center">
  <a href="https://natade-coco.com">
    <img alt="natadeCOCO Publisher" src="app/client/src/images/publisher.svg" width="160" />
  </a>
  <h1 align="center">
    Unit App Bootstrap
  </h1>
</div>

### Status: ALPHA

![platform-overview](./docs/platform-overview.png)

### Introduction

[natadeCOCO](https://natade-coco.com)では、1つのコンテナイメージにパッケージングしたWebアプリを、[配信ユニット](https://natade-coco.com/product/unit/)にアップロードすることができます。アップロード時に発行されたコードを[専用のモバイルアプリ](https://natade-coco.com/product/pocket/)で読み込むと、配信ユニットに紐づけたWi-Fiスポットに接続し、モバイルアプリ内のブラウザでWebアプリを開きます。エリア限定のWebアプリ配信ができるだけでなく、ユーザプロフィールの取得やプッシュ通知の送信、Webアプリ間のデータ共有など、アイデアの実現をサポートするさまざまな機能がプラットフォームに組み込まれているため、コンテンツづくりに専念することができます。

このリポジトリでは、モバイルアプリやプラットフォームAPIと連携する各種機能の実装例をブートストラップとして公開しています。すばらしいアイデアを実現する際のベースとして是非ご活用ください！

### Quick Start (10 minutes)

1. [管理コンソール](https://natade-coco.com/product/console/)にログインして配信ユニットを有効にする。
2. 配信ユニットにWebアプリを登録する。

```
タイトル: 任意
イメージ: registry.gitlab.com/natade-coco/apps/bootstrap:latest
ポート番号: 3030
```

3. 発行されたコードをモバイルアプリで読み取る。

### Examples

以下のトピックに関する実装例を含んでいます。

- [1. セッション](#1-%e3%82%bb%e3%83%83%e3%82%b7%e3%83%a7%e3%83%b3)
- [2. プロフィール](#2-%e3%83%97%e3%83%ad%e3%83%95%e3%82%a3%e3%83%bc%e3%83%ab)
- [3. クレジットカード情報](#3-%e3%82%af%e3%83%ac%e3%82%b8%e3%83%83%e3%83%88%e3%82%ab%e3%83%bc%e3%83%89%e6%83%85%e5%a0%b1)
- [4. イーサリアムアドレス](#4-%e3%82%a4%e3%83%bc%e3%82%b5%e3%83%aa%e3%82%a2%e3%83%a0%e3%82%a2%e3%83%89%e3%83%ac%e3%82%b9)
- [5. プッシュ通知](#5-%e3%83%97%e3%83%83%e3%82%b7%e3%83%a5%e9%80%9a%e7%9f%a5)
- [6. データストア](#6-%e3%83%87%e3%83%bc%e3%82%bf%e3%82%b9%e3%83%88%e3%82%a2)
- [7. 連携用コード](#7-%e9%80%a3%e6%90%ba%e7%94%a8%e3%82%b3%e3%83%bc%e3%83%89)
- [8. 決済用コード](#8-%e6%b1%ba%e6%b8%88%e7%94%a8%e3%82%b3%e3%83%bc%e3%83%89)

#### 1. セッション

セッションを確立するには、**オフチェインJWT** をモバイルアプリに要求し、取得したJWTを使って Server App でユーザを認証します。
セッションを確立することで、**ユーザ識別子** や **データストア** への接続情報が取得できます。

#### 2. プロフィール

ユニットアプリからモバイルアプリにユーザのプロフィールを要求することができます。要求時には **認可ダイアログ** が表示されます。

#### 3. クレジットカード情報

Pocket SDKを通してクレジットカード情報を要求することができます。取得できる情報にセキュリティコードは含まれません。要求時には **認可ダイアログ** の表示および **生体認証** が行われます。

> クレジットカード情報を取り扱う場合、「クレジットカード情報の非保持化」またはPCI DSS（Payment Card Industry Data Security Standard）への準拠が必要です。

#### 4. イーサリアムアドレス

ユーザへのインセンティブ付与などを目的として、モバイルアプリにイーサリアムアドレスを要求することができます。要求時には **認可ダイアログ** が表示されます。

> アドレスは **メインネット(Mainnet)** に属しています。トランザクション実行時はユーザに [etherscan.io](https://etherscan.io) でトランザクションの詳細を通知するようにしてください。

#### 5. プッシュ通知

アプリを配信しているナタデココスポットのエリア外にいるユーザに、プッシュ通知でお知らせすることができます。通知は送信元がアプリ、送信先がユーザとなります。

#### 6. データストア

データストアはユニット単位で利用でき、同じユニットで配信されるアプリ間でデータを共有することができます。
サンプルでは商品リストとユーザごとの買い物かごにデータストアを利用しています。

#### 7. 精算用コード

買い物かごの精算など、店舗側に購入内容を伝える必要がある場合は、**顧客側アプリ** から精算用コードを発行します。
店舗側は提示された精算用コードを読み取り、**店舗側アプリ** で精算手続きをおこないます。

#### 8. 決済用コード

店舗側アプリでコード決済に対応する場合は、JPQRの **統一動的QRコード** を生成して顧客に提示します。
顧客は対応している任意のコード決済アプリで支払うことができます。

> あらかじめ **統一店舗識別コード** の取得が必要です。詳細は [統一QR「JPQR」普及事業](https://jpqr-start.jp/) および [統一動的QRコードの仕様](https://www.paymentsjapan.or.jp/wordpress/wp-content/uploads/2020/04/MPM_Guideline_2.0.pdf) をご確認ください。

### Technical Note

#### Client App

フロントエンド(Client App)は、[GatsbyJS](https://www.gatsbyjs.org/) + [TypeScript](https://www.typescriptlang.org/) + [Redux Toolkit](https://redux-toolkit.js.org/) + [Bulma](https://bulma.io/) を利用したシングルページアプリケーションです。

#### Server App

バックエンド(Server App)は、[Feathers](https://feathersjs.com/) + [TypeScript](https://www.typescriptlang.org/) を利用したNodeアプリケーションです。

#### Datastore

配信ユニットに搭載されているデータストアの実体は [Directus](https://directus.io/) です。

#### Packages

以下は natadeCOCO でメンテナンスしているnpmパッケージです。

- [@natade-coco/pocket-sdk](https://www.npmjs.com/package/@natade-coco/pocket-sdk) #=> モバイルアプリとの連携
- [@natade-coco/hub-sdk](https://www.npmjs.com/package/@natade-coco/hub-sdk) #=> プラットフォームAPIとの連携
- [@natade-coco/jpqr](https://www.npmjs.com/package/@natade-coco/jpqr) #=> Wasm による JPQR のコード生成

#### CI/CD

コンテナイメージのビルド時に Client App をビルドし、Server App の public フォルダに配置しています。Server App は 実行時にTypescript のトランスパイルが行われます。また、リポジトリのタグを切った際、配信ユニットにデプロイするための Webhook を実行します。[Dockerfile](./app/Dockerfile) および [.gitlab-ci.yml](./.gitlab-ci.yml) をご確認ください。

### Development

以下の手順でローカルの開発環境でも Client App と Server App を連動させたデバッグがおこなえます。(プッシュ通知は利用できません。)

#### 1. 各種環境変数の設定

[ユニットアプリ環境変数](#%e3%83%a6%e3%83%8b%e3%83%83%e3%83%88%e3%82%a2%e3%83%97%e3%83%aa%e7%92%b0%e5%a2%83%e5%a4%89%e6%95%b0) を app/server/.env に保存します。

```
$ cat > app/server/.env <<EOF
DIRECTUS_URL=${YOUR_DIRECTUS_URL}
DIRECTUS_ID=${YOUR_DIRECTUS_ID}
DIRECTUS_SECRET=${YOUR_DIRECTUS_SECRET}
NATADECOCO_ID=${YOUR_NATADECOCO_ID}
NATADECOCO_SECRET=${YOUR_NATADECOCO_SECRET}
EOF
```

#### 2. データストアの準備

DirectusのAPIを使ってデータストアにサンプルで利用するコレクションを作成します。`1. 各種環境変数の設定` の.envを読み込んでいます。

```
$ cd datastore
$ npm install
$ npm run create_collections
```

#### 3. Server App の実行

`1. 各種環境変数の設定` を読み込んで開発用 Server App を起動します。開発用 Client App が Websocket で繋がります。

```
$ cd app/server
$ npm install
$ npm run develop
```

#### 4. Client App の実行

開発用 Client App を起動します。`NODE_ENV=development` の場合、GATSBY_APP_TOKEN をモバイルユーザのトークンとして扱うようにしているため、必要に応じてWebアプリのトークン(NATADECOCO_APP_TOKEN)で代用します。

```
$ cd app/client
$ npm install
$ export GATSBY_APP_TOKEN=${YOUR_APP_TOKEN} 
$ npm run develop
```

### Build & Deployment

#### ユニットアプリ環境変数

ユニットアプリの環境変数は配信ユニットで動作させる際に、自動で注入されます。実際の値は管理コンソールの[アプリ詳細] -> [基本情報] -> [環境変数] でご確認ください。

| 環境変数            | 説明                             | 参照 |
| :------------------ | :------------------------------- | :--- |
| `DIRECTUS_URL`      | データストアのURL                | 起動 |
| `DIRECTUS_ID`       | データストアのユーザID           | 起動 |
| `DIRECTUS_SECRET`   | データストアのユーザシークレット | 起動 |
| `NATADECOCO_ID`     | ユニットアプリのID               | 起動 |
| `NATADECOCO_SECRET` | ユニットアプリのシークレット     | 起動 |

#### GitLab CI/CD 環境変数(オプション)

Gitlab CI/CD 環境変数はコンテナイメージのビルドやデプロイに利用しています。クラッシュレポートの送信先設定([Sentry](https://sentry.io/))や、リポジトリのタグ作成時にWebhookを利用した配信ユニットへのデプロイが行われます。

| 環境変数                | 説明                                                                          | 参照     |
| :---------------------- | :---------------------------------------------------------------------------- | :------- |
| `CLIENT_SENTRY_DSN_URL` | Client App用のSentry DSN                                                      | ビルド   |
| `SERVER_SENTRY_DSN_URL` | Server App用のSentry DSN                                                      | ビルド   |
| `NATADECOCO_HOOK_URL`   | デプロイ用のWebhook URL([アプリ詳細] -> [外部連携] -> [Webhook URL])          | デプロイ |
| `NATADECOCO_APP_TOKEN`  | Webhook用のアクセストークン([アプリ詳細] -> [外部連携] -> [Webhook トークン]) | デプロイ |

## License

Licensed under the [MIT License](./LICENSE).