import * as React from 'react'

const Keyword = (props: any) => (
  <span className="tag is-primary is-light">{props.children}</span>
)

export default Keyword