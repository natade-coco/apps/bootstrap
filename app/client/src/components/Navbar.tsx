import * as React from 'react'
import { Link } from '@reach/router'
import brand from '../images/brand.svg'
import { REPO_URL } from '../constants'

const Navbar = () => {
  const [isActive, setisActive] = React.useState(false);

  return (
    <nav className="navbar is-fixed-top" role="navigation" aria-label="main navigation">
      <div className="navbar-brand">
        <a className="navbar-item" href="/">
          <img src={brand} width="160" alt="natadeCOCO" />
        </a>

        <a role="button"
          onClick={() => setisActive(!isActive)}
          className={`navbar-burger burger has-text-white has-text-weight-bold ${isActive ? "is-active" : ""}`}
          aria-label="menu"
          aria-expanded="false"
          data-target="navbarBasicExample">
          <span aria-hidden="true"></span>
          <span aria-hidden="true"></span>
          <span aria-hidden="true"></span>
        </a>
      </div>

      <div id="navbarBasicExample" className={`navbar-menu ${isActive ? "is-active" : ""}`}>
        <div className="navbar-start">
          <Link to='/' className="navbar-item has-text-white has-text-weight-bold is-size-7" onClick={() => setisActive(!isActive)}>ホーム</Link>
          <div className="navbar-item has-dropdown">
            <div className="navbar-link has-text-white has-text-weight-bold is-size-7">
              サンプル
            </div>
            <div className="navbar-dropdown" style={{ borderTop: "none" }}>
              <Link to='/ex/session' className="navbar-item has-text-white has-text-weight-bold is-size-7" onClick={() => setisActive(!isActive)}>1. セッション</Link>
              <Link to='/ex/profile' className="navbar-item has-text-white has-text-weight-bold is-size-7" onClick={() => setisActive(!isActive)}>2. プロフィール</Link>
              <Link to='/ex/ccinfo' className="navbar-item has-text-white has-text-weight-bold is-size-7" onClick={() => setisActive(!isActive)}>3. クレジットカード情報</Link>
              <Link to='/ex/ethaddr' className="navbar-item has-text-white has-text-weight-bold is-size-7" onClick={() => setisActive(!isActive)}>4. イーサリアムアドレス</Link>
              <Link to='/ex/notification' className="navbar-item has-text-white has-text-weight-bold is-size-7" onClick={() => setisActive(!isActive)}>5. プッシュ通知</Link>
              <Link to='/ex/datastore' className="navbar-item has-text-white has-text-weight-bold is-size-7" onClick={() => setisActive(!isActive)}>6. データストア</Link>
              <Link to='/ex/sharing' className="navbar-item has-text-white has-text-weight-bold is-size-7" onClick={() => setisActive(!isActive)}>7. 精算用コード</Link>
              <Link to='/ex/payoff' className="navbar-item has-text-white has-text-weight-bold is-size-7" onClick={() => setisActive(!isActive)}>8. 決済用コード</Link>
            </div>
          </div>
        </div>
        <div className="navbar-end">
          <div className="navbar-item">
            <p className="control">
              <a href={REPO_URL}>
                <button className="button is-small is-info">
                  <span className="icon">
                    <i className="fas fa-code"></i>
                  </span>
                  <span><strong>ソースコードをみる</strong></span>
                </button>
              </a>
            </p>
          </div>
        </div>
      </div>
    </nav>
  )
}

export default Navbar