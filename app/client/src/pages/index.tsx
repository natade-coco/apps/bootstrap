import React from 'react'

import Layout from "../components/Layout"
import Navbar from "../components/Navbar"
import Router from "../components/Router"

import Welcome from "../features/ex/Welcome"
import EstablishSession from "../features/ex/EstablishSession"
import GetProfile from "../features/ex/GetProfile"
import GetCCInfo from "../features/ex/GetCCInfo"
import GetEthAddr from "../features/ex/GetEthAddr"
import IssueNotification from "../features/ex/IssueNotification"
import UseDatastore from "../features/ex/UseDatastore"
import IssueConnection from "../features/ex/IssueConnection"
import IssueJPQR from "../features/ex/IssueJPQR"

import "./index.scss"

const Page = () => {
  return (
    <Layout>
      <Navbar />
      <Router>
        <Welcome path="/" />
        <EstablishSession path="/ex/session" />
        <GetProfile path="/ex/profile" />
        <GetCCInfo path="/ex/ccinfo" />
        <GetEthAddr path="/ex/ethaddr" />
        <IssueNotification path="/ex/notification" />
        <UseDatastore path="/ex/datastore" />
        <IssueConnection path="/ex/sharing" />
        <IssueJPQR path="/ex/payoff" />
      </Router>
    </Layout>
  )
}
export default Page