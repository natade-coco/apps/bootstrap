import { SDK } from '@directus/sdk-js'
import { IConfigurationOptions } from '@directus/sdk-js/dist/types/Configuration'

let client: SDK;

const COLLECTION_ITEM = 'items'
export interface Item {
  id: string;
  status: string;
  created_on: string;
  modified_on: string;
  name: string;
  price: number;
  description: string;
  image: any;
}

const COLLECTION_CART = 'carts'
export interface Cart {
  id?: string;
  created_on?: string;
  modified_on?: string;
  customer_id: string;
  is_settled: boolean;
}

const COLLECTION_CART_ITEM = 'cart_items'
export interface CartItem {
  id: string;
  created_on: string;
  modified_on: string;
  cart_id: string;
  item_id: string;
}

export const newClient = (options: IConfigurationOptions) => {
  client = new SDK(options)
}

const getClient = async () => {
  await client.refreshIfNeeded()
  return client
}

// Item

export const getItems = async () => {
  const ds = await getClient()
  const result = await ds.getItems<Item[]>(COLLECTION_ITEM)
  const joined = await Promise.all(result.data.map(async (item) => {
    const itemResult = await getItem(item.id)
    return itemResult
  }))
  return joined
}

export const getItem = async (item_id: string) => {
  const ds = await getClient()
  const result = await ds.getItem<Item>(COLLECTION_ITEM, item_id)
  const fileResult = await ds.getFile(String(result.data.image))
  result.data.image = (fileResult.data.data as any)
  return result.data
}

// File

export const getFile = async (file_id: string) => {
  const ds = await getClient()
  const result = await ds.getFile(String(file_id))
  return (result.data.data as any)
}

// Cart

export const getCart = async (customer_id: string) => {
  const ds = await getClient()
  const result = await ds.getItems<Cart[]>(COLLECTION_CART, {
    filter: {
      customer_id: {
        eq: customer_id
      },
      is_settled: {
        eq: false
      }
    }
  })
  if (result.data.length !== 0) {
    return result.data[0]
  }
  const createResult = await ds.createItem<Cart>(COLLECTION_CART, { customer_id, is_settled: false })
  return createResult.data
}

export const settleCart = async (cart_id: string) => {
  const ds = await getClient()
  const result = await ds.updateItem<Partial<Cart>>(COLLECTION_CART, cart_id, { is_settled: true })
  return result.data
}

// CartItem

export const getCartItems = async (cart_id: string) => {
  const ds = await getClient()
  const result = await ds.getItems<CartItem[]>(COLLECTION_CART_ITEM, {
    filter: {
      cart_id: {
        eq: cart_id
      }
    }
  })
  return result.data
}

export const addItemTo = async (cart_id: string, item_id: string) => {
  const ds = await getClient()
  const result = await ds.createItem<Partial<CartItem>>(COLLECTION_CART_ITEM, { cart_id, item_id })
  return result.data
}

export const removeItem = async (cart_item_id: string) => {
  const ds = await getClient()
  await ds.deleteItem(COLLECTION_CART_ITEM, cart_item_id)
  return true
}
