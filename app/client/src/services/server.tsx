import io from 'socket.io-client'
import feathers from '@feathersjs/feathers'
import socketio from '@feathersjs/socketio-client'

interface Session {
  token: string | null
  user: User | null
  app: App | null
  datastore: Config | null
}

interface User {
  '_id': string | null
}

interface App {
  '_id': string | null
}

interface Config {
  url: string | null
  project: string | null
  mode: 'jwt' | 'cookie'
  persist: boolean
  tokenExpirationTime: number
  token: string | null
  localExp: number
}

export { Session }

const NewFeathersApp = () => {
  if (typeof window === "undefined" || !window.document) {
    return null
  }
  const socket = io(process.env.GATSBY_API_URL || window.location.origin);
  const app = feathers();
  app.configure(socketio(socket, {
    timeout: 5000
  }));
  return app
}

const app = NewFeathersApp();

export const getSession = (jwt?: string): Promise<Session> => {
  return app.service('authentication').create({ strategy: 'natadecoco', token: jwt });
}

export const issueNotification = (recipient: string, title: string, message: string, url: string): Promise<any> => {
  return app.service('messages').create({ recipient, title, message, url });
}
