import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { RootState } from '../../app/rootReducer'
import { fetchProfile, contextChanged } from './slice'
import Keyword from '../../components/Keyword'
import Ex from '../../components/Ex'
import IconPlaceholder from '../../images/coco.svg'

const Feature = (props: any) => (
  <div className="section">
    <div className="columns">
      <div className="column is-full">
        <Summary />
      </div>
      <div className="column is-full">
        <GetProfile />
      </div>
    </div>
  </div>
)

export default Feature

const Summary = () => (
  <div className="container">
    <h1 className="subtitle">
      <strong>2. プロフィール</strong>
    </h1>
    <p>
      ユニットアプリからモバイルアプリにユーザのプロフィールを要求することができます。
      要求時には <Keyword>認可ダイアログ</Keyword> が表示されます。
    </p>
  </div>
)

const GetProfile = () => {
  const dispatch = useDispatch()

  const ex = useSelector((state: RootState) => state.ex)

  useEffect(() => {
    dispatch(contextChanged(''))
  }, [])

  const onClick = (e: React.MouseEvent<HTMLInputElement>) => {
    const scope = ['name', 'fullName', 'email', 'tel', 'image']
    dispatch(fetchProfile(scope))
  }

  const maskEmail = (raw: string) => {
    if (!raw) {
      return ''
    }
    const acc = raw.match(/(.*)@/)[1]
    const domain = raw.match(/@.*/)[0]
    return acc.substr(0, 2) + '*'.repeat(acc.length - 2) + domain
  }

  const maskNumber = (raw: string) => {
    if (!raw) {
      return ''
    }
    return '*'.repeat(raw.length - 4) + raw.substr(-4)
  }

  return (
    <div className="container">
      <div className="field">
        <Ex category="サンプル" name="プロフィールの要求" />
      </div>
      <div className="field is-grouped is-grouped-centered">
        <figure className="image is-128x128">
          <img className="is-rounded has-background-white" src={ex.image || IconPlaceholder} alt="icon" />
        </figure>
      </div>
      <div className="field">
        <div className="control is-expanded">
          <input className="input is-small" type="text" placeholder="ニックネーム" readOnly value={ex.name || ""} />
        </div>
      </div>
      <div className="field">
        <div className="control is-expanded">
          <input className="input is-small" type="text" placeholder="フルネーム" readOnly value={ex.fullName || ""} />
        </div>
      </div>
      <div className="field">
        <div className="control is-expanded">
          <input className="input is-small" type="email" placeholder="Eメール" readOnly value={maskEmail(ex.email) || ""} />
        </div>
      </div>
      <div className="field">
        <div className="control is-expanded">
          <input className="input is-small" type="tel" placeholder="電話番号" readOnly value={maskNumber(ex.tel) || ""} />
        </div>
      </div>
      <div className="field">
        <div className="control is-expanded">
          <button onClick={onClick} className={`button is-small is-fullwidth is-info ${ex.isLoading ? "is-loading" : ""}`}>
            <strong>実行</strong>
          </button>
        </div>
        <p className="help is-danger">{ex.error}</p>
      </div>
    </div>
  )
}
