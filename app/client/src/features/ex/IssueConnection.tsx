import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { RootState } from '../../app/rootReducer'
import { showConnection, contextChanged } from './slice'
import Keyword from '../../components/Keyword'
import Ex from '../../components/Ex'

const Feature = (props: any) => (
  <div className="section">
    <div className="columns">
      <div className="column is-full">
        <Summary />
      </div>
      <div className="column is-full">
        <IssueConnection />
      </div>
    </div>
  </div>
)

export default Feature

const Summary = () => (
  <div className="container">
    <h1 className="subtitle">
      <strong>7. 精算用コード</strong>
    </h1>
    <p>
      買い物かごの精算など、店舗側に購入内容を伝える必要がある場合は、<Keyword>顧客側アプリ</Keyword> から精算用コードを発行します。
    </p>
    <p>
      店舗側は提示された精算用コードを読み取り、<Keyword>店舗側アプリ</Keyword> で精算手続きをおこないます。
    </p>
  </div>
)

interface ConnectionForm {
  target: string
  path: string
  allow: string
}

const IssueConnection = () => {
  const dispatch = useDispatch()
  const ex = useSelector((state: RootState) => state.ex)
  const initialForm = {
    target: ex.session?.app._id,
    path: '/ex/payoff',
    allow: ex.session?.user._id
  }
  const [form, setForm] = useState<ConnectionForm>(initialForm)

  useEffect(() => {
    dispatch(contextChanged(''))
  }, [])

  const onClick = (e: React.MouseEvent<HTMLInputElement>) => {
    dispatch(showConnection(form.target, form.path, [form.allow]))
  }

  const onChangeForm = (e: React.ChangeEvent<HTMLInputElement>) => {
    switch (e.target.id) {
      case 'target':
        setForm({ ...form, target: e.target.value })
        break
      case 'path':
        setForm({ ...form, path: e.target.value })
        break
      case 'allow':
        setForm({ ...form, allow: e.target.value })
        break
    }
  }

  const onClickModalClose = (e: React.MouseEvent<HTMLInputElement>) => {
    dispatch(contextChanged(''))
  }

  return (
    <div className="container">
      <div className="field">
        <Ex category="サンプル" name="精算用コードの表示" />
      </div>
      <div className="field">
        <label className="label is-small">店舗側アプリの識別子</label>
        <div className="control is-expanded">
          <input className="input is-small" type="text" id="target" value={form.target} onChange={onChangeForm} />
        </div>
      </div>
      <div className="field">
        <label className="label is-small">店舗側アプリのパス</label>
        <div className="control is-expanded">
          <input className="input is-small" type="text" id="path" value={form.path} onChange={onChangeForm} />
        </div>
      </div>
      <div className="field">
        <label className="label is-small">アクセス可能とするユーザの識別子</label>
        <div className="control is-expanded">
          <input className="input is-small" type="text" id="allow" value={form.allow} onChange={onChangeForm} />
        </div>
      </div>
      <div className="field">
        <div className="control is-expanded">
          <button onClick={onClick} className={`button is-small is-fullwidth is-info ${ex.isLoading ? "is-loading" : ""}`}>
            <strong>実行</strong>
          </button>
        </div>
        <p className="help is-danger">{ex.error}</p>
      </div>
      <div className={`modal ${ex.connect ? 'is-active' : ''}`}>
        <div className="modal-background"></div>
        <div className="modal-content has-text-centered">
          <div className="subtitle">精算用コード</div>
          <figure className="image is-square">
            <img src={ex.connect} />
          </figure>
        </div>
        <button className="modal-close is-large" aria-label="close" onClick={onClickModalClose}></button>
      </div>
    </div>
  )
}
