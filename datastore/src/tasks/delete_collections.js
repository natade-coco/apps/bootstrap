(async function () {
  const config = require('../config');
  const client = await require('../client');

  config.collection_names.forEach(async (name) => {
    try {
      await client.deleteCollection(name);
    } catch (err) {
      console.log(err);
    }
  });
})();
