(async function () {
  const config = require('../config');
  const client = await require('../client');

  config.collection_schemas.forEach(async (schema) => {
    try {
      const result = await client.createCollection(schema);
      console.log(result);
    } catch (err) {
      console.log(err);
    }
  });
})();
