// collection schemas
const items = require('../data/collections/items.json');
const carts = require('../data/collections/carts.json');
const cartItems = require('../data/collections/cart_items.json');
module.exports.collection_schemas = [items, carts, cartItems];

// collection names
module.exports.collection_names = ["items", "carts", "cart_items"];
